Tool to interact with ROMable WonderWitch images.

This is quite messy and hacky, but it does work on the 3 original ROMable WonderWitch images. It's not very good at detecting bad inputs or handling them, so try to be gentle.

# Easy Mode

- Place a ROMable WonderWitch image in this directory, named `work.ws`. This can be either release of Judgement Silversword, or Dicing Knight.
- Place the entire set of files required for the program into one folder.
- Rename the main executable of the program to either "BOOT_V.FX" or "BOOT_H.FX" (vertical or horizontal orientation, respectively)
- Then drag the folder onto `build.bat`.
- `work.ws` is now updated with the new program.
