# ROM-WITCH - Tool to interact with ROMable WonderWitch images
# Copyright (C) 2019 Alex "trap15" Marshall <trap15@raidenii.net>
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

DEBUG	?= 0

CC	?= gcc
RM	?= rm

OUTPUT	?= romwitch

OBJECTS	?=
OBJECTS	+= romwitch.o

OBJECTS	:= $(addprefix obj/,$(OBJECTS))

CFLAGS	?= -Wall -Wextra -Wno-unused-label -Wno-unused-parameter -Wno-variadic-macros
LDFLAGS	?=

ifeq ($(DEBUG),1)
CFLAGS	+= -g -DDEBUG=1
LDFLAGS	+= -g
else
CFLAGS	+= -O2 -DDEBUG=0
endif

.PHONY: all clean

all: $(OBJECTS) $(OUTPUT)

clean:
	$(RM) -f $(OUTPUT) $(OBJECTS)

obj/%.o: src/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<

$(OUTPUT): $(OBJECTS)
	@mkdir -p $(@D)
	$(CC) -o $@ $+ $(LDFLAGS)

test:
	mednafen ./work.ws
