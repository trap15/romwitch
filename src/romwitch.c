/*
ROM-WITCH - Tool to interact with ROMable WonderWitch images
Copyright (C) 2019 Alex "trap15" Marshall <trap15@raidenii.net>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#define LOG(args...) do { fprintf(stderr, args); fflush(stderr); } while(0)
#if DEBUG
# define DBG LOG
#else
# define DBG(args...)
#endif
#define ERROR(X...) do { LOG(X); exit(1); } while(0)

FILE *wsfp;
#define TELL() ftell(wsfp)
#define SEEK(DST) fseek(wsfp, DST, SEEK_SET)
#define SEEK_REL(DST) fseek(wsfp, DST, SEEK_CUR)
#define RD8() ({ uint8_t v; fread(&v, 1, 1, wsfp); v; })
#define RD16() ({ uint16_t v; fread(&v, 2, 1, wsfp); v; })
#define RD32() ({ uint32_t v; fread(&v, 4, 1, wsfp); v; })
#define WR8(VAL) do { uint8_t v = VAL; fwrite(&v, 1, 1, wsfp); } while(0)
#define WR16(VAL) do { uint16_t v = VAL; fwrite(&v, 2, 1, wsfp); } while(0)
#define WR32(VAL) do { uint32_t v = VAL; fwrite(&v, 4, 1, wsfp); } while(0)

typedef struct {
  char name[0x10];            // Name of file
  char info[0x18];            // Extra info for the file
                              // DIR: Name of FsIL
                              // LINK: Target file name

  uint32_t data_lptr;         // FILE: Contents far pointer
                              // DIR: Location of FS entries far pointer

  uint32_t size;              // FILE: Contents length
                              // DIR: Entry count * 4 (?)

  uint16_t count;             // FILE: Number of blocks
                              // DIR: Entry count
                              // -1 indicates free entry

  uint16_t mode;              // Access flags, filetypes, etc

  uint32_t mtime;             // Last modification time

  union {
    uint32_t appid;           // FILE: Application ID
    uint32_t il_lptr;         // DIR: Far pointer to FsIL
  };

  uint32_t resource;          // FILE: Resource start position
                              //       -1 indicates no resource
                              // DIR: Free area start offset
} RootFsEntry;

#define FS_DATA_BASE_SEG 0x8426
#define MAX_FS_ENTRIES 256

#define LPTR2LIN(PTR) ((((PTR) >> 16) << 4) + ((PTR) & 0xFFFF))

uint32_t fs_entry_base;
int fs_entry_count;
RootFsEntry fs_entries[MAX_FS_ENTRIES];
void *fs_ent_data[MAX_FS_ENTRIES];
char initial_prg[64] = "foo";
bool is_vertical;

void fs_write(void) {
  int oldpos = TELL();

  uint16_t seg = FS_DATA_BASE_SEG;
  for(int i = 0; i < fs_entry_count; i++) {
    SEEK((seg << 4) - 0x80000);
    fs_entries[i].data_lptr = seg << 16;
    fwrite(fs_ent_data[i], fs_entries[i].size, 1, wsfp);
    seg += (fs_entries[i].size + 0xF) >> 4;
  }
  SEEK((seg << 4) - 0x80000);

  fwrite(fs_entries, fs_entry_count * sizeof(RootFsEntry), 1, wsfp);
  uint16_t count = fs_entry_count;
  fwrite(&count, 2, 1, wsfp);
  fwrite(initial_prg, strlen(initial_prg) + 1, 1, wsfp);

  uint8_t pad = 0xFF;
  while(TELL() < 0x70000) {
    fwrite(&pad, 1, 1, wsfp);
  }

  uint16_t fs_meta_off = fs_entry_count * sizeof(RootFsEntry);

  SEEK(0x2F0 + 1);
  WR16(seg);
  SEEK(0x33E + 1);
  WR16(seg);
  SEEK(0x1F22 + 1);
  WR16(seg);
  SEEK(0x1F2A + 1);
  WR16(seg);

  SEEK(0x2F4 + 1);
  WR16(fs_meta_off + 2);
  SEEK(0x342 + 1);
  WR16(fs_meta_off + 2);
  SEEK(0x1F2F + 2);
  WR16(fs_meta_off);

  SEEK(0x7FFFC);
  WR8(4 + (is_vertical ? 1 : 0));

  SEEK(oldpos);
}

#define MK_FP(seg, off) (((seg) << 16) | (off))

void fs_load(void) {
  int oldpos = TELL();

  SEEK(0x7FFFC);
  is_vertical = !!(RD8() & 1);

  SEEK(0x1F2A+1);
  fs_entry_base = RD16() << 16;
  SEEK(0x1F2F+2);
  uint16_t fs_meta_off = RD16();

  SEEK(LPTR2LIN(fs_entry_base + fs_meta_off) - 0x80000);
  fs_entry_count = RD16();
  DBG("FS entry count: %d [from %04X:%04X]\n", fs_entry_count, fs_entry_base >> 16, fs_meta_off);
  char *prg_ptr = initial_prg;
  char ch = '\0';
  do {
    ch = RD8();
    *(prg_ptr++) = ch;
  } while(ch != '\0');

  SEEK(oldpos);
}

void fs_read(void) {
  fs_load();

  uint32_t lptr = fs_entry_base;
  int count = fs_entry_count;

  int oldpos = TELL();
  SEEK(LPTR2LIN(lptr) - 0x80000);
  for(int i = 0; i < count; i++) {
    RootFsEntry st;

    fread(&st, sizeof(RootFsEntry), 1, wsfp);

    fs_entries[i] = st;
  }

  for(int i = 0; i < count; i++) {
    SEEK(LPTR2LIN(fs_entries[i].data_lptr) - 0x80000);
    fs_ent_data[i] = malloc(fs_entries[i].size);
    fread(fs_ent_data[i], fs_entries[i].size, 1, wsfp);
  }

  SEEK(oldpos);
}

void clear_fs(char **args) {
  fs_read();

  fs_entry_count = 0;
  fs_write();
}

void setinit_fs(char **args) {
  fs_read();

  strcpy(initial_prg, args[0]);
  fs_write();
}

void add_file(const char *srcname, bool set_initial) {
  DBG("Adding file '%s'\n", srcname);

  FILE *fp = fopen(srcname, "rb");
  uint8_t head[0x40];
  fread(head, 0x40, 1, fp);
  if(memcmp(head, "#!ws", 4) != 0) {
    LOG("'%s' not a WonderWitch file!\n", srcname);
    fclose(fp);
    return;
  }

  RootFsEntry *ent = &fs_entries[fs_entry_count];
  fread(ent, sizeof(RootFsEntry), 1, fp);
  LOG("Adding: name='%s' info='%s' size=%d\n", ent->name, ent->info, ent->size);
  fs_ent_data[fs_entry_count] = malloc(ent->size);
  fread(fs_ent_data[fs_entry_count], ent->size, 1, fp);
  fclose(fp);

  if(set_initial) {
    sprintf(initial_prg, "/rom0/%s", ent->name);
  }

  fs_entry_count++;
}

void add_fs(char **args) {
  fs_read();

  while(*args != NULL) {
    add_file(*args, false);
    args++;
  }
  fs_write();
}

void extract_file(const char *srcname, const char *dstname) {
  int did_write = 0;
  for(int i = 0; i < fs_entry_count; i++) {
    if(strcmp(fs_entries[i].name, srcname) == 0) {
      FILE *fp = fopen(dstname, "wb");
      char fr_header[0x40];
      memset(fr_header, 0xFF, sizeof(fr_header));
      memcpy(fr_header, "#!ws", 4);
      fwrite(fr_header, sizeof(fr_header), 1, fp);
      RootFsEntry ent = fs_entries[i];
      ent.data_lptr = 0;
      fwrite(&ent, sizeof(RootFsEntry), 1, fp);
      fwrite(fs_ent_data[i], fs_entries[i].size, 1, fp);
      fclose(fp);
      did_write = 1;
      break;
    }
  }

  if(!did_write) {
    ERROR("Couldn't find source file! '%s'\n", srcname);
  }
}

void copy_fs(char **args) {
  fs_read();

  while(*args != NULL) {
    char *param;
    param = strchr(*args, '=');
    if(param == NULL) {
      ERROR("Bad file specifier\n");
    }
    // split
    *(param++) = '\0';

    extract_file(*args, param);

    args++;
  }
}

void indent(int depth) {
  while(depth--) {
    LOG("|   ");
  }
}

#define FMODE_X       (0x0001)  /* executable */
#define FMODE_W       (0x0002)  /* writable */
#define FMODE_R       (0x0004)  /* readable */
#define FMODE_MMAP    (0x0008)  /* disallow mmap */
#define FMODE_STREAM  (0x0010)  /* StreamIL instance */
#define FMODE_ILIB    (0x0020)  /* IL instance */
#define FMODE_LINK    (0x0040)  /* symbolic link */
#define FMODE_DIR     (0x0080)  /* directory */

void mode2str(int mode, char str[]) {
  char *ptr = str;
  *(ptr++) = (mode & 0x0080) ? 'D' : '-';
  *(ptr++) = (mode & 0x0004) ? 'R' : '-';
  *(ptr++) = (mode & 0x0002) ? 'W' : '-';
  *(ptr++) = (mode & 0x0001) ? 'X' : '-';
  *(ptr++) = (mode & 0x0008) ? 'M' : '-';
  *(ptr++) = (mode & 0x0010) ? 'S' : '-';
  *(ptr++) = (mode & 0x0020) ? 'I' : '-';
  *(ptr++) = (mode & 0x0040) ? 'L' : '-';
  *(ptr++) = 0;
}

void fs_walk(int depth, uint32_t lptr, int count) {
  if(!(lptr & 0x80000000)) {
    indent(depth); LOG("X-- Not in ROM\n");
    return;
  }
  int oldpos = TELL();
  SEEK(LPTR2LIN(lptr) - 0x80000);
  for(int i = 0; i < count; i++) {
    RootFsEntry st;
    char modestr[16];

    fread(&st, sizeof(RootFsEntry), 1, wsfp);
    if((st.count == 0xFFFF) || (st.name[0] == '\xFF') ||
       (st.name[0] == '\0' && st.size == 0)) {
      indent(depth); LOG(">-- Unused\n");
      continue;
    }

    time_t mtime = st.mtime;
    struct tm t = *gmtime(&mtime);
    // Epoch was moved forward 30 years for these
    t.tm_year += 30;
    char timestr[128];
    strftime(timestr, sizeof(timestr), "%a %b %d %H:%M:%S %Y", &t);
    mode2str(st.mode, modestr);

    indent(depth); LOG(">-- NAME: '%s'\n", st.name);
    indent(depth); LOG("|   INFO: '%s'\n", st.info);
    indent(depth); LOG("|   DATA: 0x%08X [%d bytes, %d count]\n", st.data_lptr, st.size, st.count);
    indent(depth); LOG("|   MODE: %s (%04Xh)\n", modestr, st.mode);
    indent(depth); LOG("|   TIME: %s (%08Xh)\n", timestr, st.mtime);
    indent(depth); LOG("|   ILPT: 0x%08X\n", st.il_lptr);
    indent(depth); LOG("|   RSRC: 0x%08X\n", st.resource);
    if((st.mode & FMODE_DIR) &&
       strcmp(st.name, ".") != 0 &&
       strcmp(st.name, "..") != 0) {
      indent(depth); LOG("|   |\n");
      int count = st.count;
      if(st.resource != 0xFFFFFFFF) {
        count = st.resource;
      }
      fs_walk(depth + 1, st.data_lptr, count);
    }else{
      indent(depth); LOG("|\n");
    }
  }
  SEEK(oldpos);
}

void list_fs(char **args) {
  // Internal FS
  //fs_walk(0, 0x83BE0000, 0x10);

  fs_load();
  fs_walk(0, fs_entry_base, fs_entry_count);
}

void ls_fs(char **args) {
  fs_read();

  for(int i = 0; i < fs_entry_count; i++) {
    LOG("%s\n", fs_entries[i].name);
  }
}

void extract_fs(char **args) {
  fs_read();

  char basepath[256];
  char outpath[256];
  strcpy(basepath, args[0]);
  if(strlen(basepath) == 0) {
    ERROR("Specify a path\n");
    return;
  }

  if(basepath[strlen(basepath) - 1] != '/') {
    strcat(basepath, "/");
  }

  for(int i = 0; i < fs_entry_count; i++) {
    char *extension;
    if(fs_entries[i].mode & FMODE_ILIB) {
      extension = "IL";
    }else if(fs_entries[i].mode & FMODE_X) {
      extension = "FX";
    }else{
      extension = "FR";
    }
    char *name = fs_entries[i].name;
    if(stricmp(name, initial_prg + strlen("/rom0/")) == 0) {
      name = is_vertical ? "BOOT_V" : "BOOT_H";
    }else if(name[0] == '@') {
      name++;
    }
    sprintf(outpath, "%s%s.%s", basepath, name, extension);
    LOG("'%s' (%s) -> '%s'\n", fs_entries[i].name, fs_entries[i].info, outpath);
    extract_file(fs_entries[i].name, outpath);
  }
}

void build_fs(char **args) {
  fs_read();
  // Clear
  fs_entry_count = 0;

  while(*args != NULL) {
    char *path = *args;
    char *dirsep = strrchr(path, '/');
    if(!dirsep) {
      dirsep = strrchr(path, '\\');
    }
    char fname[256];
    if(dirsep) {
      strcpy(fname, dirsep + 1);
    }else{
      strcpy(fname, path);
    }
    bool is_boot = false;
    if(stricmp(fname, "BOOT_H.FX") == 0) {
      is_boot = true;
      is_vertical = false;
    }else if(stricmp(fname, "BOOT_V.FX") == 0) {
      is_boot = true;
      is_vertical = true;
    }
    add_file(path, is_boot);
    args++;
  }

  fs_write();
}

void usage(const char *app) {
  LOG("Usage:\n"
      "        %s work.ws <command> [command args]\n"
      "Commands:\n"
      "  set_init [filename]        Set the initial executable path.\n"
      "  clear                      Empty the filesystem.\n"
      "  list                       List the filesystem.\n"
      "  ls                         List the files.\n"
      "  rm [filenames]             Remove files.\n"
      "  add [filename]             Add a file.\n"
      "  copy [srcfile=filename]    Copy a file from the filesystem.\n"
      "  extract [outpath]          Copy filesystem to directory.\n"
      "  build [path]               Build filesystem from flattened directories and files.\n"
      "\n"
      "Example:\n"
      "  romwitch work.ws copy BOSS01=BOSS01.FR\n",
      app);
}

int main(int argc, char *argv[]) {
  LOG("ROMWITCH (C)2019 Alex 'trap15' Marshall\n");
  if(argc < 3) {
    usage(argv[0]);
    return 1;
  }

  const char *arg_ws = argv[1];
  const char *arg_cmd = argv[2];
  char **args = argv + 3;

  wsfp = fopen(arg_ws, "rb+");

  if(strcmp(arg_cmd, "clear") == 0) {
    clear_fs(args);
  }else if(strcmp(arg_cmd, "list") == 0) {
    list_fs(args);
  }else if(strcmp(arg_cmd, "ls") == 0) {
    ls_fs(args);
  }else if(strcmp(arg_cmd, "copy") == 0) {
    copy_fs(args);
  }else if(strcmp(arg_cmd, "add") == 0) {
    add_fs(args);
  }else if(strcmp(arg_cmd, "set_init") == 0) {
    setinit_fs(args);
  }else if(strcmp(arg_cmd, "extract") == 0) {
    extract_fs(args);
  }else if(strcmp(arg_cmd, "build") == 0) {
    build_fs(args);
  }else{
    ERROR("Invalid command '%s'\n", arg_cmd);
    usage(argv[0]);
    return 1;
  }

  fclose(wsfp);

  return 0;
}
